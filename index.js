const spec = [{
	name: 'Beer 1',
	minTemperature: 4,
	maxTemperature: 6,
}, {
	name: 'Beer 2',
	minTemperature: 5,
	maxTemperature: 6,
}, {
	name: 'Beer 3',
	minTemperature: 4,
	maxTemperature: 7,
}, {
	name: 'Beer 4',
	minTemperature: 6,
	maxTemperature: 8,
}, {
	name: 'Beer 5',
	minTemperature: 3,
	maxTemperature: 5,
}]

module.exports = (data) => {
	const results = []

	data.forEach(sampleBeer => {
		const specBeer = spec.find(beer => beer.name === sampleBeer.name)
		let withInTemperature

		if (sampleBeer.temperature > specBeer.maxTemperature || sampleBeer.temperature < specBeer.minTemperature) {
			withInTemperature = false
		} else {
			withInTemperature = true
		}

		results.push({
			name: sampleBeer.name,
			withInTemperature,
			temperature: sampleBeer.temperature,
			minTemperature: specBeer.minTemperature,
			maxTemperature: specBeer.maxTemperature
		})
	})

	return results
}

module.exports.spec = spec
