The way how I understand the test:

1. A function (`./index.js`) that takes some beer data (beer name and temperature) as input, and output if it's out of the temperature range.

1. I have built a UI which takes some random data (`./data.js`) and output the results. If the temperature is in red, it's out of the temperature range. If it's green, it's fine. Just run `node cli`

1. A test that tests with sample data once. (`npm run testOnce`)

1. Because the data is random so `npm test` runs the tests 10 times.
