const randomInt = (min, max) => Math.floor(Math.random() * (max - min + 1) + min);

module.exports = [{
	name: 'Beer 1',
	temperature: randomInt(1, 9)
}, {
	name: 'Beer 2',
	temperature: randomInt(1, 9)
}, {
	name: 'Beer 3',
	temperature: randomInt(1, 9)
}, {
	name: 'Beer 4',
	temperature: randomInt(1, 9)
}, {
	name: 'Beer 5',
	temperature: randomInt(1, 9)
}]
