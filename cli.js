const chalk = require('chalk')

const data = require('./data')

const test = require('./')

const results = test(data)

results.forEach(result => {
	console.log(`${result.name}: `)
	if (result.withInTemperature) {
		console.log(`${chalk.green(`${result.temperature}°C`)}, within the range of ${result.minTemperature}°C and ${result.maxTemperature}°C`)
	} else {
		console.log(`${chalk.red(`${result.temperature}°C`)}, not within the range of ${result.minTemperature}°C and ${result.maxTemperature}°C`)
	}
	console.log()
})
