const assert = require('assert')

const spec = require('./').spec

const test = require('./')

const data = require('./data')

const results = test(data)

results.forEach(result => {
	const specBeer = spec.find(beer => beer.name === result.name)

	if (result.withInTemperature) {
		assert(result.temperature >= specBeer.minTemperature, `${result.temperature} >= ${specBeer.minTemperature}`)
		assert(result.temperature <= specBeer.maxTemperature, `${result.temperature} <= ${specBeer.minTemperature}`)
	} else {
		assert(result.temperature < specBeer.minTemperature || result.temperature > specBeer.maxTemperature)
	}

	assert.strictEqual(result.minTemperature, specBeer.minTemperature)
	assert.strictEqual(result.maxTemperature, specBeer.maxTemperature)
})
